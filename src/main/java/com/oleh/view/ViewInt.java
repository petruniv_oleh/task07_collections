package com.oleh.view;

public interface ViewInt {
    void menuMap();
    void addToTree();
    void removeFromTree();
    void printTree();
}
