package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.tree.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {
    private Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    Scanner sc = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(View.class.getName());

    enum MenuItems {
        ADD("1"), REMOVE("2"), PRINT("3"), QUIT("Q");
        private String menuKey;

        MenuItems(String menuKey) {
            this.menuKey = menuKey;
        }

        public String getMenuKey() {
            return menuKey;
        }
    }

    public View() {
        controller = new Controller();
        controller.createTree(new BinaryTree<Integer, String>());
        menuMap();
        mapMenuShow();

    }

    @Override
    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put(MenuItems.ADD.getMenuKey(), "1. Add to tree");
        menuMap.put(MenuItems.REMOVE.getMenuKey(), "2. Remove from tree");
        menuMap.put(MenuItems.PRINT.getMenuKey(), "3. Print tree");

        menuMapMethods.put(MenuItems.ADD.getMenuKey(), this::addToTree);
        menuMapMethods.put(MenuItems.REMOVE.getMenuKey(), this::removeFromTree);
        menuMapMethods.put(MenuItems.PRINT.getMenuKey(), this::printTree);

    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    private void mapMenuShow() {
        String key;
        Scanner scanner = new Scanner(System.in);
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals(MenuItems.QUIT.getMenuKey()));
    }


    @Override
    public void addToTree() {
        System.out.print("Key: ");
        String key = sc.nextLine();
        System.out.print("Value: ");
        String value = sc.nextLine();
        controller.addToTree(key, value);
        logger.info("Adding to tree value " + value + " with key " + key);
        mapMenuShow();
    }

    @Override
    public void removeFromTree() {

        System.out.println("Enter key to remove: ");
        String key = sc.nextLine();
        logger.info("removing from the tree value for key: " + key);
        controller.removeFromTree(key);
        mapMenuShow();
    }

    @Override
    public void printTree() {
        logger.info("Printing tree");
        controller.printTree();
        mapMenuShow();
    }
}
