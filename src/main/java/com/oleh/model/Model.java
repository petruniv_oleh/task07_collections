package com.oleh.model;

import com.oleh.model.tree.BinaryTree;

public class Model implements ModelInt{
    BinaryTree binaryTree;

    @Override
    public <K extends Comparable, V> void createTree(BinaryTree<K, V> myTree) {
        binaryTree = myTree;
    }

    @Override
    public <K extends Comparable, V> void addToTree(K key, V value) {
        binaryTree.put(key, value);
    }

    @Override
    public <K extends Comparable> void removeFromTree(K key) {
        binaryTree.remove(key);
    }

    @Override
    public void printTree() {
        binaryTree.printTree();
    }
}
