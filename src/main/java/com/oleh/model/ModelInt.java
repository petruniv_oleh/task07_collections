package com.oleh.model;

import com.oleh.model.tree.BinaryTree;

public interface ModelInt {
    <K extends Comparable,V> void createTree(BinaryTree<K,V> myTree);
    <K extends Comparable,V> void addToTree(K key, V value);
    <K extends Comparable> void removeFromTree(K key);
    void printTree();

}
