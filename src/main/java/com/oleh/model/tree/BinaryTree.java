package com.oleh.model.tree;


import java.util.Comparator;
import java.util.Objects;

public class BinaryTree<K extends Comparable, V> implements BinaryTreeMap<K, V> {
    class Node<K extends Comparable, V> implements Comparable<K> {
        K key;
        V value;
        Node leftChild;
        Node rightChild;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public int compareTo(K o) {
            return key.compareTo(o);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?, ?> node = (Node<?, ?>) o;
            return Objects.equals(key, node.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "key=" + key.toString() +
                    ", value=" + value.toString() +
                    ", right child key = "+(rightChild==null ? "null":rightChild.key.toString())+
                    ", left child key = "+(leftChild==null ? "null":leftChild.key.toString())+
                    '}';
        }
    }


    private Node root;

    @Override
    public void put(K key, V value) {
        Node<K, V> newNode = new Node<K, V>(key, value);
        if (root == null) {
            root = newNode;
        } else {
            Node<K, V> current = root;
            Node<K, V> parent;
            while (true) {
                parent = current;
                if (newNode.compareTo(current.key) < 0) { //go left
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = newNode;
                        return;
                    }

                }
            }
        }
    }

    @Override
    public V getValue(K key) {
        if (root == null) {
            return null;
        }

        Node<K, V> current = root;
        while (!current.key.equals(key)) {
            if (key.compareTo(current.key) < 0) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
            if (current == null) {
                return null;
            }
        }
        return current.value;
    }

    @Override
    public boolean remove(K key) {
        if (root == null) {
            return false;
        }

        boolean isLeftChild = true;
        Node<K, V> nodeToDelete = root;
        Node<K, V> parent = nodeToDelete;
        while (!nodeToDelete.key.equals(key)) {
            parent = nodeToDelete;
            if (key.compareTo(nodeToDelete.key) < 0) {
                isLeftChild = true;
                nodeToDelete = nodeToDelete.leftChild;
            } else {
                isLeftChild = false;
                nodeToDelete = nodeToDelete.rightChild;
            }
        }
        if (nodeToDelete == null) {
            return false;
        }

        if (nodeToDelete.rightChild == null && nodeToDelete.leftChild == null) { // if no child
            if (nodeToDelete == root) {
                root = null;
                return true;
            } else {
                if (isLeftChild) {
                    parent.leftChild = null;
                    return true;
                } else {
                    parent.rightChild = null;
                    return true;
                }

            }
        } else if (nodeToDelete.rightChild == null) { //if have left child
            if (nodeToDelete == root) {
                root = nodeToDelete.leftChild;
                return true;
            }
            if (isLeftChild) {
                parent.leftChild = nodeToDelete.leftChild;
                return true;
            } else {
                parent.rightChild = nodeToDelete.leftChild;
                return true;
            }
        } else if (nodeToDelete.leftChild == null) { // if have right child
            if (nodeToDelete == root) {
                root = nodeToDelete.rightChild;
                return true;
            }
            if (isLeftChild) {
                parent.leftChild = nodeToDelete.rightChild;
                return true;
            } else {
                parent.rightChild = nodeToDelete.rightChild;
                return true;
            }
        } else {
            Node<K, V> successor = getSuccessor(nodeToDelete);
            if (nodeToDelete == root) {
                root = successor;
                return true;
            } else if (isLeftChild) {
                parent.leftChild = successor;
            } else {
                parent.rightChild = successor;
            }
            successor.leftChild = nodeToDelete.leftChild;
        }
        return true;

    }

    private Node<K, V> getSuccessor(Node<K, V> nodeToDelete) {
        Node<K, V> sParent = nodeToDelete;
        Node<K, V> currentNode = nodeToDelete.rightChild;
        Node<K, V> successor = nodeToDelete;

        while (currentNode != null) {
            sParent = successor;
            successor = currentNode;
            currentNode = currentNode.rightChild;
        }

        if (!successor.equals(nodeToDelete.rightChild)) {
            sParent.leftChild = successor.rightChild;
            successor.rightChild = nodeToDelete.rightChild;

        }

        return successor;
    }

    @Override
    public void printTree() {
        print(root);
    }

    private void print(Node<K, V> node) {
        System.out.println(node);
        if (node.leftChild != null) {
            print(node.leftChild);
        }
        if (node.rightChild != null) {
            print(node.rightChild);
        }
    }
}
