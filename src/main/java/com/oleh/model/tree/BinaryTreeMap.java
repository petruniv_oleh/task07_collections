package com.oleh.model.tree;

public interface BinaryTreeMap<K extends Comparable, V> {
    void put(K key, V value);
    V getValue(K key);
    boolean remove(K key);
    void printTree();
}
