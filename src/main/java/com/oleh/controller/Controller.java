package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.tree.BinaryTree;

public class Controller implements ControllerInt {

    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public <K extends Comparable, V> void createTree(BinaryTree<K, V> myTree) {
        model.createTree(myTree);
    }

    @Override
    public <K extends Comparable, V> void addToTree(K key, V value) {
        model.addToTree(key, value);
    }

    @Override
    public <K extends Comparable> void removeFromTree(K key) {
        model.removeFromTree(key);
    }

    @Override
    public void printTree() {
        model.printTree();
    }
}
